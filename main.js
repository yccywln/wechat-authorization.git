
// #ifndef VUE3
import Vue from 'vue'
import App from './App'
import store from './state/index'
import { getImg } from "./tool/index.js"
Vue.prototype.$store = store
Vue.prototype.$getImg = getImg
Vue.config.productionTip = false
import uView from 'uview-ui';
Vue.use(uView);
import {share} from './mixins/index.js'
Vue.mixin(share)
App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
// #endif

// // #ifdef VUE3
// import { createSSRApp } from 'vue'
// import App from './App.vue'
// export function createApp() {
//   const app = createSSRApp(App)
//   return {
//     app
//   }
// }
// // #endif