import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
const store = new Vuex.Store({
    state: {
		loginState:false
	},
    mutations: {
		loginFun(state,type){
			state.loginState = type
		}
	},
    actions: {}
})
export default store

