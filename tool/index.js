export let getImg = async (fileID) => {
	if(fileID === undefined) return
	if (fileID.indexOf("cloud://") != -1) {
		let res = await uniCloud.getTempFileURL({
			fileList: [fileID]
		});
		return await res.fileList[0].download_url
	} else {
		return fileID
	}
	// console.log(res,1111122223333)
}
