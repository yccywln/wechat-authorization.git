/**
 * 表格时间格式化
 */
export function formatDate(cellValue) {
	if (cellValue == null || cellValue == "") return "";
	var date = new Date(cellValue)
	var year = date.getFullYear()
	var month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
	var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
	var hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours()
	var minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()
	var seconds = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()
	return year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds
}
// 格式化日期
export function Date_nhms(value) {
	let date = new Date(value);
	let y = date.getFullYear();
	let MM = date.getMonth() + 1;
	MM = MM < 10 ? ('0' + MM) : MM;
	let d = date.getDate();
	d = d < 10 ? ('0' + d) : d;
	return y + '-' + MM + '-' + d + ' ';
}

export const msg = (title, icon = 'none', duration = 1500) => {
	uni.showToast({
		title: title,
		icon: icon,
		duration: duration
	});


	// uni.showToast({
	//     title,
	//     duration,
	// 		icon,
	// 		fail(err){
	// 			console.log(err)
	// 		}
	// });
}

export const to = (url, data, animationType = 'pop-in', animationDuration = 300) => {
	url += (url.indexOf('?') < 0 ? '?' : '&') + param(data)
	uni.navigateTo({
		url,
		animationType,
		animationDuration,
		success: function(res) {

		},
		fail: function(e) {
			console.log(e)
		}
	})
}
// export const to_shut = (url, data,animationType = 'pop-in', animationDuration = 300) => {
// 	url += (url.indexOf('?') < 0 ? '?' : '&') + param(data)
// 	uni.navigateTo({
// 		url,
// 		animationType,
// 		animationDuration,
// 		success: function (res) {

// 		},
// 		fail: function (e) {
// 			console.log(e)
// 		}
// 	})
// }
// 对象转换成 get 传参格式
export function param(data) {
	let url = ''
	for (var k in data) {
		let value = data[k] !== undefined ? data[k] : ''
		url += '&' + k + '=' + encodeURIComponent(value)
	}
	return url ? url.substring(1) : ''
}


export function set(name, data) {
	return uni.setStorageSync(name, data);
};

export function get(name) {
	let str = uni.getStorageSync(name);
	return str

};
export function remove(name) {
	return uni.removeStorageSync(name);
}
