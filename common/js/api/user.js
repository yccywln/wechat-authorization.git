import {
	set,
	get,
	to,
	msg
} from "../uilt/index.js"
export function register() {

	return new Promise((resolve, reject) => {
		uni.getUserProfile({
			desc: "获取用户资料",
			success: (res) => {
				resolve(res.userInfo)
			},
			fail: () => {
				reject('请点击，允许按钮')
			}
		})
	}).then(async res => {
		userReplenish(res)
		return res

	})
}
export let userReplenish = async (res, type) => {
	type = type || false
	res = await axios('user/replenish', res)
	if (res.result.code == 0) {
		const delta = 1
		if (!type) {
			uni.navigateBack({
				delta: delta,
				success: () => {
					const pages = getCurrentPages() // 获取当前页面栈数组，第一个元素为首页，最后一个元素为当前页面  
					let page = pages[Math.max(pages.length - 1 - delta, 0)] // 要返回到的页面，超过页面栈，则为首页  
					page.onLoad(page.options) // 通过调用页面的 onLoad 里面的处理逻辑实现刷新数据  
				}
			})
		}else{
			
			return getdata()
		}

	}
}
async function axios(url, data) {
	let res = await uniCloud.callFunction({
		name: "index",
		data: {
			url: url,
			data
		}
	})
	return res
}
// 用户登录
export async function obtain() {
	return new Promise(resolve => {
		uni.login({
			provider: "weixin",
			success: async (loginRes) => {
				let res = await axios('user/loginWeixin', loginRes.code)
				resolve(res)
				console.log(res)
				set('sessionKey',res.result.sessionKey)
				set('uni_id_token', res.result.token)
				set('uni_id_token_expired', res.result.tokenExpired)
			},
		});
	})
}

// 获取用户信息
export let getdata = () => axios('user/getUser')
// 获取手机号 登录换取
export let codeGetMobile = data => axios('user/getMobile',data)
// 直接解码手机号
export let getMobile = data => axios('user/getMobile',data)
// userReplenish()
// 用户登录
// obtain()
