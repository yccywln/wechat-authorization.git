import {
	get
} from "@/common/js/uilt/index.js"
export default {
	created() {

		wx.showShareMenu({
			withShareTicket: true,
			menus: ['shareAppMessage', 'shareTimeline']
		});

	},
	// 小程序分享
	onShareAppMessage() {
		let token = get('uni_id_token')
		// console.log(mpId._id)
		return {
			provider: "weixin",
			title: 'test',
			path: 'pages/index/index?token='+token,
			imageUrl: "https://lmg.jj20.com/up/allimg/4k/s/02/2109242332225H9-0-lp.jpg",
			desc: "测试中请",
			
		}
	}
}
