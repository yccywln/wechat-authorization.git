import deploy from "@/uni-starter.config.js"
const db = uniCloud.database();
export default async function() {
	//clientDB的错误提示
	function onDBError({
		code, // 错误码详见https://uniapp.dcloud.net.cn/uniCloud/clientdb?id=returnvalue
		message
	}) {
		console.log('onDBError', {
			code,
			message
		});
		// 处理错误
		console.error(code, message);
		if ([
				'TOKEN_INVALID_INVALID_CLIENTID',
				'TOKEN_INVALID',
				'TOKEN_INVALID_TOKEN_EXPIRED',
				'TOKEN_INVALID_WRONG_TOKEN',
				'TOKEN_INVALID_ANONYMOUS_USER'
			].includes(code)) {
			uni.navigateTo({
				url: '/SubPacKages/login/login'
			})
		}
	}
	// 绑定clientDB错误事件
	db.on('error', onDBError)
	
	// 解绑clientDB错误事件
	//db.off('error', onDBError)
	db.on('refreshToken', function({
		token,
		tokenExpired
	}) {
		console.log('监听到clientDB的refreshToken', {
			token,
			tokenExpired
		});
		// store.commit('user/login', {
		// 	token,
		// 	tokenExpired
		// })
	})
	
	
	
	const {
		"router": {
			needLogin,
			visitor,
		}
	} = deploy //需要登录的页面
	let list = ["navigateTo", "redirectTo", "reLaunch", "switchTab"];

	list.forEach(item => {
		uni.addInterceptor(item, {
			invoke(e) { // 调用前拦截
				//获取用户的token
				const token = uni.getStorageSync('uni_id_token'),
					//token是否已失效
					tokenExpired = uni.getStorageSync('uni_id_token_expired') < Date.now(),
					//获取要跳转的页面路径（url去掉"?"和"?"后的参数）
					url = e.url.split('?')[0];
				//获取要前往的页面路径（即url去掉"?"和"?"后的参数）
				const pages = getCurrentPages();
				if (!pages.length) {
					console.log("首页启动调用了");
					return e
				}
				const fromUrl = pages[pages.length - 1].route;
				let inLoginPage = fromUrl.split('/')[2] == 'login'
				console.log(fromUrl.split('/')[2])
				//判断当前窗口是否为登录页面，如果是则不重定向路由 
				if (!inLoginPage && url == "/SubPacKages/login/login") {
					// console.log(6664444)
				} else {
					let pass = true
					if (needLogin) {
						pass = needLogin.every((item) => {
							if (typeof(item) == 'object' && item.pattern) {
								return !item.pattern.test(url)
							}
							return url != item
						})
					}
					if (visitor) {
						pass = visitor.some((item) => {
							if (typeof(item) == 'object' && item.pattern) {
								return item.pattern.test(url)
							}
							return url == item
						})
					}

					if (!pass && (token == '' || tokenExpired)) {
						uni.showToast({
							title: '请先登录哦',
							icon: 'none'
						})

						let url = "/SubPacKages/login/login"
						setTimeout(() => {
							uni.navigateTo({
								url
							})
						}, 700)
						return false
					}
				}
				return e
			},
			fail(err) { // 失败回调拦截
				console.log(err);
				if (Debug) {
					console.log(err);
					uni.showModal({
						content: JSON.stringify(err),
						showCancel: false
					});
				}
			}
		})
	})
}
